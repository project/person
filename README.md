# Person Configuration Kit

This module creates a system for storing and displaying people on your website.
Common examples where this is needed are a staff directory, faculty list, or
board of directors page.

In addition to creating a content type for managing people, this also provides a
view for displaying them, sorted alphabetically by last name.

An optional submodule is provided to allow better formtting, though currently
the changes it provides are minimal. This module is optimized for the Olvero
theme, but may be compatible with other themes too.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/person).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/person).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Auto Entity Label module, but if you will want to
customize the way titles are generated (for example to include a title such as
"Dr." or degrees) then the inclusion of the Token module is also recommended.


## Installation

- Install the Person module as you would normally install a contributed Drupal
  module. Visit `https://www.drupal.org/node/1897420` for further information.
  We strongly recommend using composer to ensure all dependencies will be
  handled automatically.
- This will import configuration. Once installed, the module doesn't really
  provide any additional functionality, so it can be uninstalled. Note that
  you won't be able to install it again on the same site, unless you delete the
  Person bundle and People view that were installed originally.
- There is also a Person Olivero submodule that is recommended if using this
  module on a site using the Olivero theme. If using a custom theme, you may
  need to implement similar CSS to what is found in the submodule.


## Configuration

1. When installed, the provided People view will have two displays: one for a
   standard list display, and the other a `"glossary"` view showing people whose
   last names begin with a specific letter. It's likely that you site only needs
   one of these, so disable or delete the version you don't intend to use.
2. You may want to customize what information is captured and displayed for the
   Person nodes on your site. If so, add fields according to the documentation
   at `https://www.drupal.org/docs/7/nodes-content-types-and-fields/add-a-field-to-a-content-type`


## Maintainers

- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
- chrislynch42 - [chrislynch42](https://www.drupal.org/user/42169)